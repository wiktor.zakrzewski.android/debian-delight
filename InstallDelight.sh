#! /bin/bash
# This is the Debian Delight installation Script
# if git wont work: copy folder InstallDelight to ~
# run it from ~
# Version 0.1
# Remember to run it only on Debian.
# Designed for Debian 11 "Bullseye" (stable, non-free)
# Might work on "Bookworm" or ubuntu, but who knows...
# additionally build from source:
# jonaburg/picom xdgmenumaker
#
sudo apt update && sudo apt upgrade
# sudo apt install git
# git clone https://gitlab.com/LinuxRant/debian-delight.git
# cosmetic, tty font setup:
sudo apt install xfonts-terminus pc-fonts fonts-font-awesome fonts-noto fonts-hack fonts-liberation -y
sudo dpkg-reconfigure -plow console-setup
# Prepare the Graphical Desktops
sudo apt install xorg xserver-xorg xutils mesa-utils xinit -y
sudo apt install libpangocairo-1.0-0 -y
sudo apt install lightdm slick-greeter lightdm-settings -y
#sudo apt install firmware-linux firmware-linux-nonfree -y

# Environment Utilities wifi, bt, audio etc...
sudo apt install cups bluez blueman network-manager-gnome cbatticon light tint2 jgmenu feh nitrogen suckless-tools pulseaudio lxappearance alsa-utils volumeicon-alsa pavucontrol-qt dunst notify-osd xdotool -y
sudo systemctl enable bluetooth
sudo systemctl enable cups
sudo apt install qt5ct lxqt-config-brightness
#add: qt-settings maybe resign from qt5ct?, policykit?
#############################################
# let's install 1st window manager JWM		#
sudo apt install jwm -y
cp ~/debian-delight/unstable/.jwmrc ~/
#TOUCH FRIENDLY APPS:
# sudo apt install onboard
#############################################
# Now the 2nd window manager QTILE			#
sudo apt install python3-pip python3-xcffib python3-cairocffi -y
sudo pip install qtile
sudo cp ~/debian-delight/unstable/.config/qtile/qtile.desktop /usr/share/xsessions/qtile.desktop
cp ~/debian-delight/unstable/.config/qtile/config.py ~/.config/qtile/
cp ~/debian-delight/unstable/.config/qtile/autostart.sh
cp ~/debian-delight/unstable/.config/qtile/qtile-restart-picom.sh
#############################################
# Now for the i3-gaps window manager		#
# Dependancies:
sudo apt install meson dh-autoreconf libxcb-keysyms1-dev libpango1.0-dev libxcb-util0-dev xcb libxcb1-dev libxcb-icccm4-dev libyajl-dev libev-dev libxcb-xkb-dev libxcb-cursor-dev libxkbcommon-dev libxcb-xinerama0-dev libxkbcommon-x11-dev libstartup-notification0-dev libxcb-randr0-dev libxcb-xrm0 libxcb-xrm-dev libxcb-shape0 libxcb-shape0-dev i3status -y
sudo apt install libnotify-bin unzip simple-scan
#i3-Gaps:
cd Downloads
git clone https://github.com/Airblader/i3 i3-gaps
cd i3-gaps
mkdir -p build && cd build
meson --prefix /usr/local
ninja
sudo ninja install

#Package management
sudo apt install gnome-software-manager flatpak gnome-software-plugin-flatpak -y
flatpak remote-add --if-not-exists flathub https://flathub.org/repo/flathub.flatpakrepo
#Programs
sudo apt install qterminal pcmanfm-qt firefox-esr jackd qjackctl lmms qsynth -y
#TUI PROGRAMS
sudo apt install ranger micro cmus htop neofetch -y
# Copy/replace (Install) configs, settings, images, fonts, icons, lmms projects
bash /debian-delight/delight-configs.sh

# Lets install Ibhagwan Picom compositor for rounded corners and blur fx from source
#Dependancies:
sudo apt install libxext-dev libxcb1-dev libxcb-damage0-dev libxcb-xfixes0-dev libxcb-shape0-dev libxcb-render-util0-dev libxcb-render0-dev libxcb-randr0-dev libxcb-composite0-dev libxcb-image0-dev libxcb-present-dev libxcb-xinerama0-dev libxcb-glx0-dev libpixman-1-dev libdbus-1-dev libconfig-dev libgl-dev libegl-dev libpcre2-dev libpcre3-dev libevdev-dev uthash-dev libev-dev libx11-xcb-dev meson git
#building
git clone https://github.com/ibhagwan/picom.git
cd picom
git submodule update --init --recursive
meson --buildtype=release . build
ninja -C build
#installation
ninja -C build install
cd ..
#YOU HAVE TO MANUALLY REMOVE PICOM GIT CLONED FOLDER IN YR HOME FOLDER
