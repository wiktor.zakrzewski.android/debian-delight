#!/bin/bash
# this script will backup your dotfiles as they are, 
# into the git folder ~/debian-delight/unstable
echo "Starting debian-delight backup version 0.1" &&
echo "copying files to the local git folder in:" &&
echo "~/debian-delight/unstable/" &&
sleep 4
# qtile configs
cp ~/.config/qtile/config.py ~/debian-delight/unstable/.config/qtile/ -v &&
cp ~/.config/qtile/qtile-restart-picom.sh ~/debian-delight/unstable/.config/qtile/ -v &&
echo "present qtile configs have been copied to the debian-delight/unstable/ folder" &&
# jwm
cp ~/.jwmrc ~/debian-delight/unstable/ -v &&
echo "present JWM config(.jwmrc) has been copied to the debian-delight/unstable/ folder (without icons) "
# jwm icons
# tint2rc
cp ~/.config/tint2/delight-qtile-deepin-tandem1-dark.tint2rc ~/debian-delight/unstable/.config/tint2/ -v &&
cp ~/.config/tint2/delight-qtile-deepin-tandem1-light-noblur.tint2rc ~/debian-delight/unstable/.config/tint2/ -v &&
echo "tint2 configs have been copied to the debian-delight/unstable/ folder (without icons)"
# tint2 icons
#i3
cp ~/.config/i3/config  ~/debian-delight/unstable/.config/i3/ -v &&
cp /etc/i3status.conf  ~/debian-delight/unstable/etc/ -v &&
echo "i3 configs have been copied to the debian-delight/unstable/ folder"
#Additional files and settings
# ibhagwan/picom config
cp ~/.config/picom.conf ~/debian-delight/unstable/.config/ -v &&
echo "picom has been copied to the debian-delight/unstable/ folder"
# useful scripts

cp delight-theming.sh ~/debian-delight/unstable/ -v &&
cp games-fullscale.sh ~/debian-delight/unstable/ -v &&
cp huion-pen.sh ~/debian-delight/unstable/ -v &&
cp PreInstallDelight.sh ~/debian-delight/unstable/ -v &&
cp InstallDelight.sh ~/debian-delight/unstable/ -v &&
cp landscape.sh ~/debian-delight/unstable/ -v &&
cp monitors.sh ~/debian-delight/unstable/ -v &&
cp extmon.sh ~/debian-delight/unstable/ -v &&
cp portraitr.sh ~/debian-delight/unstable/ -v &&
cp portraitl.sh~/debian-delight/unstable/ -v &&
echo "Helpful Scripts have been copied to debian-delight/unstable/ folder"
# jgmenu config
cp ~/.config/jgmenu/jgmenurc ~/debian-delight/unstable/ -v &&
echo "Done."
# elinks config
cp ~/.elinks/elinks.conf ~/debian-delight/unstable/ -v &&
echo "elinks config has been copied to the debian-delight/unstable/ folder"
# dunst config
cp ~/.config/dunst/dunstrc ~/debian-delight/unstable/ -v &&
cp ~/.config/dunst/linux-rant-os ~/debian-delight/unstable/ -v &&
echo "dunst configs have been copied to the debian-delight/unstable/ folder"
#To be added
# qt5ct config
# lxappearance config
echo "It seems all configs have been copied to debian-delight backup folder"
echo "you are ready to check if all files are in place"
echo "and enjoy your hoppin!"
