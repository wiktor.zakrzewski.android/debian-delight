#!/bin/bash
# this script will install delight configs from backed up dotfiles
# from cloned GitLab repo, and it will copy necessary jpegs and pngs
# for themes to work. (jwm, qtile, tint2) 
#from the git folder ~/debian-delight/unstable onto respective catalogues
echo "Starting debian-delight configs-install version 0.1" &&
echo "copying files from the local git folder in:" &&
echo "~/debian-delight/unstable/" &&
sleep 4
# qtile configs
cp ~/debian-delight/unstable/.config/qtile/config.py ~/.config/qtile/ -v &&
cp ~/debian-delight/unstable/.config/qtile/qtile-restart-picom.sh ~/.config/qtile/ -v &&
echo "present qtile configs have been copied to the debian-delight/unstable/ folder" &&
sleep 1
# jwm
cp ~/debian-delight/unstable/.jwmrc ~/ -v &&
cp ~/debian-delight/unstable/.config/jwm/* ~/.config/jwm/
echo "present JWM config(.jwmrc) has been copied to the debian-delight/unstable/ folder (without icons) "
sleep 1
# jwm icons
# tint2rc
cp ~/debian-delight/unstable/.config/tint2/* ~/.config/tint2/ -v &&
echo "tint2 configs have been copied to the debian-delight/unstable/ folder (without icons)"
sleep 1
# tint2 icons
#i3
cp ~/debian-delight/unstable/.config/i3/config  ~/.config/i3/ -v &&
cp ~/debian-delight/unstable/etc/i3status.conf  /etc/ -v &&
echo "i3 configs have been copied to the debian-delight/unstable/ folder"
sleep 1
#Additional files and settings
# ibhagwan/picom config
cp ~/debian-delight/unstable/.config/picom.conf ~/.config/ -v &&
echo "picom.conf has been copied from the debian-delight/unstable/ folder"
sleep 1
# useful scripts

cp ~/debian-delight/unstable/*.sh ~/ -v &&
echo "Helpful Scripts have been copied from debian-delight/unstable/ to home folder"
sleep 1
# jgmenu config
mkdir ~/.config/jgmenu -v 
cp ~/debian-delight/unstable/.config/jgmenu/jgmenurc ~/.config/jgmenu/jgmenurc -v &&
echo "Done."
sleep 1
# elinks config
mkdir ~/.elinks -v
cp ~/debian-delight/unstable/elinks.conf ~/.elinks/elinks.conf -v &&
echo "elinks config has been copied to the .elinks folder"
sleep 1
# dunst config
cp ~/debian-delight/unstable/.config/dunst/dunstrc ~/.config/dunst/dunstrc -v &&
cp ~/debian-delight/unstable/.config/dunst/linux-rant-os ~/.config/dunst/linux-rant-os -v &&
echo "dunst configs have been copied to the .config/dunst folder"
sleep 1
#To be added
# qt5ct config
# lxappearance config
echo "It seems all configs have been copied from debian-delight backup folder"
echo "go on, check all window managers!"
echo "and enjoy your hoppin!"
