#! /bin/bash

# This is first Pre Installation Script for Debian Delight
# it is required only if you have Created Root User Account
# we will need to create sudo for the user
# version 0.1
# Unfortunately before running the main script we have to install sudo and reboot machine.
su
usermod -aG sudo $USER
apt update
apt install sudo
systemctl reboot
