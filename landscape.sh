#!/bin/bash
#landscape (normal)
xrandr -o normal
xinput set-prop "Wacom MultiTouch Sensor Finger touch" --type=float "Coordinate Transformation Matrix" 0 0 0 0 0 0 0 0 0
xinput set-prop "Wacom MultiTouch Sensor Pen stylus" --type=float "Coordinate Transformation Matrix" 0 0 0 0 0 0 0 0 0
nitrogen --restore
