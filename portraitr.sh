#!/bin/sh
#portrait (right)
xrandr -o right
xinput set-prop "Wacom MultiTouch Sensor Finger touch" --type=float "Coordinate Transformation Matrix" 0 1 0 -1 0 1 0 0 1
xinput set-prop "Wacom MultiTouch Sensor Pen stylus" --type=float "Coordinate Transformation Matrix" 0 1 0 -1 0 1 0 0 1
nitrogen --set-auto
