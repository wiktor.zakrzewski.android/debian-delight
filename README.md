## Debian Delight is a work in progress RICE.
Debian Delight is an installation script, which you should launch on a fresh install on Debian Minimal. 
It prepares your desktop gui with 3 window managers which will be ready to work and will not need for customization,
much in the spirit of a full desktop environment like Gnome or KDE. It a selection of wallpapers, icons, cursors, and riced config files. 
But remember, going window manager only gives you amazing features, but for a price,
you will not be held by your hand.
But Debian Delight tries to help anyway. DEBIAN DELIGHT IS IN PRE-ALPHA STAGES.
Initially it was designed for Debian 11, but most of the installation commands work on Debian 12.
You should skim through the script yourself before you run it just to be sure that you like everything that gets installed. 
### Features:
#### 1. 200-300 MB ram used at cold boot
#### 2. Beautiful transparent, blurred terminals and apps with blurred corners'
#### 3. runs on toasters 
#### 4. apt and flatpak package managers (debian stable)
#### 5. Window Managers Only JWM, QTILE and i3-GAPS
#### 6. Touch-device friendly (if you have 2in1 device, you will be able to use it)
##### (screen rotation, touch keyboard, quick-access functions, working gestures*)
#### 7. TUI friendly workflow (you can do anything in a cool termianl)
##### (ranger, cmus, elinks etc...)
* - feature not ready	
' - feature in progress

## Installation
1. Download Debian iso and flash it to your usb
2. Install Debian Standard without root
	(so you have sudo)
3. Type this into your tty: 
> sudo apt install git
3. And this:
> git clone this-repo-https
4. then go to this:
> cd debian-delight
5. And run the install script:
> sudo bash InstallDelight.sh
6. When asked, choose utf-8/terminus/your-preffered-tty-font-size.
7. Now you have installed window managers but probably without configs
8. So you might also need to launch this to install delight configs:
> bash delight-configs.sh
> Actually you might need to read the script and copy configs manually, because of permission problems I don't know how to go around this yet.
9. Reboot

### The Project is in very early stages. (Version 0.1)
### You still have to install additional software, and customize stuff.
### (But at least it installs the most basic and tedious stuff)

to be added:
Priorities:
	installing configs(delight-configs.sh) permission issue fix
	optimized qtile configuration(few bugs here and there)
	proper picom version and config
	pcmanfm-qt setup issues fix	
	bloom icons and cursor(from deepin)
	qterminal, gtk, qt theme setup(it is there but it is not set)
	slick-greeter theme
Secondary:
	startup throbber
	grub theming
	tui/ncurses theme-switcher

Debian Delight is not a Distro, someday maybe it will be.
If it will become a distro, think of it as Debian Minimal with Qtile, JWM and i3-gaps distro, 
that aims to look as awesome as DeepinOS, but have tiling wm functionality.
Oh, and it aims to be touch friendly,
so, If you have a 2in1 device, goodbye Gnome!
