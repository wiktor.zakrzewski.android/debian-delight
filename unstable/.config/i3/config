# Should you change your keyboard layout some time, delete
# this file and re-run i3-config-wizard(1).
# i3 config file (v4)
# Please see https://i3wm.org/docs/userguide.html for a complete reference!

# Change below "Mod4" to "Mod1",
# if you want to use alt instead of Windows/Super Key
# but why would you want that, I don't know.

set $mod Mod4

# Font for window titles. Will also be used by the bar unless a different font
# is used in the bar {} block below.
font pango:Ubuntu Mono Bold 14

# Pango allows for additional features to fonts (like little images), nice nerdy feature.
font pango:Ubuntu Mono Bold 14

# position of the window title if applicable
title_align center

# The mouse middle button over a titlebar/border kills the window
bindsym --release --border button2 kill

# window borders type/width "normal <nr>" - with titles
# "pixel <nr>" - without titles, pixel eliminates titlebars
# <nr> - width in pixels
default_border pixel 4

#the same but for a floating windows
default_floating_border pixel 4

# enable floating mode for all XTerm windows
#for_window [class="XTerm"] floating enable
for_window [class="Nitrogen"] floating enable
for_window [class="blueman-manager"] floating enable
for_window [class="Blueman-manager"] floating enable
# The combination of xss-lock, nm-applet and pactl is a popular choice

# Below html codes for Window Border Colors
# class                         border	          background        text            indicator       child_border
client.focused                  #ff00ff         #ff00ff         #00000055         #ff00ff       #ff00ff
#																								last.focused.inactive
client.focused_inactive         #ff00ff00         #00990000         #00000055       #ff00ff       #ff00ff00
client.unfocused                #ff00ff00         #ff00ff         #00000055         #ff00ff       #00000000
client.urgent                   #ff4400         #ff4400         #00000055         #ff4400       #ff440066
client.placeholder              #000000         #ffffff         #00000055         #ffcc00       #ffffff66

# xss-lock grabs a logind suspend inhibit lock and will use i3lock to lock the
# screen before suspend. Use loginctl lock-session to lock your screen.
exec --no-startup-id xss-lock --transfer-sleep-lock -- i3lock --nofork

# Use light to adjust screen brightness
bindsym XF86MonBrightnessUp exec light -s sysfs/backlight/auto -A 5
bindsym XF86MonBrightnessDown exec light -s sysfs/backlight/auto -U 5

# Use pactl to adjust volume in PulseAudio.
set $refresh_i3status killall -SIGUSR1 i3status
bindsym XF86AudioRaiseVolume exec --no-startup-id pactl set-sink-volume @DEFAULT_SINK@ +10% && $refresh_i3status
bindsym XF86AudioLowerVolume exec --no-startup-id pactl set-sink-volume @DEFAULT_SINK@ -10% && $refresh_i3status
bindsym XF86AudioMute exec --no-startup-id pactl set-sink-mute @DEFAULT_SINK@ toggle && $refresh_i3status
bindsym XF86AudioMicMute exec --no-startup-id pactl set-source-mute @DEFAULT_SOURCE@ toggle && $refresh_i3status

# Use Mouse+$mod to drag floating windows to their wanted position
floating_modifier $mod

# start a terminal
#bindsym $mod+Return exec i3-sensible-terminal
#bindsym $mod+t exec i3-sensible-terminal
bindsym $mod+t exec qterminal
bindsym $mod+x exec jgmenu_run

# kill focused window
bindsym $mod+q kill

# start dmenu (a program launcher)
bindsym $mod+d exec --no-startup-id dmenu_run -fn "Hack-16:bold" -sf "#000" -sb "#f8f" -nb "#438" -nf "#f8f"

# start picom compositor with blur
bindsym $mod+p exec picom --experimental-backends --corner-radius 25

# turn off picom comositor for gaming or gpu work
bindsym $mod+Shift+p exec killall picom

# A more modern dmenu replacement is rofi:
# bindcode $mod+40 exec "rofi -modi drun,run -show drun"
# There also is i3-dmenu-desktop which only displays applications shipping a
# .desktop file. It is a wrapper around dmenu, so you need that installed.
# bindcode $mod+40 exec --no-startup-id i3-dmenu-desktop

# Moving workspaces between screens mod+p
# bindsym $mod+p move workspace to output right
# bindsym $mod+Shift+w move workspace to output right

# change focus
bindsym $mod+j focus left
bindsym $mod+k focus down
bindsym $mod+l focus up
bindsym $mod+semicolon focus right

# alternatively, you can use the cursor keys:
bindsym $mod+Left focus left
bindsym $mod+Down focus down
bindsym $mod+Up focus up
bindsym $mod+Right focus right

# move focused window
bindsym $mod+Shift+j move left
bindsym $mod+Shift+k move down
bindsym $mod+Shift+l move up
bindsym $mod+Shift+semicolon move right

# alternatively, you can use the cursor keys:
bindsym $mod+Shift+Left move left
bindsym $mod+Shift+Down move down
bindsym $mod+Shift+Up move up
bindsym $mod+Shift+Right move right

# split in horizontal orientation
bindsym $mod+h split h

# split in vertical orientation
bindsym $mod+v split v

# enter fullscreen mode for the focused container
bindsym $mod+f fullscreen toggle

# change container layout (stacked, tabbed, toggle split)
bindsym $mod+s layout stacking
bindsym $mod+w layout tabbed
bindsym $mod+e layout toggle split

# toggle tiling / floating
bindsym $mod+space floating toggle

# change focus between tiling / floating windows
bindsym $mod+Shift+space focus mode_toggle

# focus the parent container
bindsym $mod+a focus parent

# focus the child container
#bindsym $mod+d focus child

# Define names for default workspaces for which we configure key bindings later on.
# We use variables to avoid repeating the names in multiple places.
set $ws1 "1:Xinfu"
set $ws2 "2:Washudi"
set $ws3 "3:Shunoya"
set $ws4 "4:Tiampo"
set $ws5 "5:Kureo"
set $ws6 "6:Tsunaho"
set $ws7 "7:Vendaya"
set $ws8 "8:Korassa"
set $ws9 "9:Lambdo"
set $ws10 "10:Tamaho"

# switch to workspace
bindsym $mod+1 workspace number $ws1
bindsym $mod+2 workspace number $ws2
bindsym $mod+3 workspace number $ws3
bindsym $mod+4 workspace number $ws4
bindsym $mod+5 workspace number $ws5
bindsym $mod+6 workspace number $ws6
bindsym $mod+7 workspace number $ws7
bindsym $mod+8 workspace number $ws8
bindsym $mod+9 workspace number $ws9
bindsym $mod+0 workspace number $ws10

# move focused container to workspace
bindsym $mod+Shift+1 move container to workspace number $ws1
bindsym $mod+Shift+2 move container to workspace number $ws2
bindsym $mod+Shift+3 move container to workspace number $ws3
bindsym $mod+Shift+4 move container to workspace number $ws4
bindsym $mod+Shift+5 move container to workspace number $ws5
bindsym $mod+Shift+6 move container to workspace number $ws6
bindsym $mod+Shift+7 move container to workspace number $ws7
bindsym $mod+Shift+8 move container to workspace number $ws8
bindsym $mod+Shift+9 move container to workspace number $ws9
bindsym $mod+Shift+0 move container to workspace number $ws10

# reload the configuration file
bindsym $mod+Shift+c reload
# restart i3 inplace (preserves your layout/session, can be used to upgrade i3)
bindsym $mod+Shift+r restart
# exit i3 (logs you out of your X session)
bindsym $mod+Shift+e exec "i3-nagbar -t warning -m 'You pressed the exit shortcut. Do you really want to exit i3? This will end your X session.' -B 'Yes, exit i3' 'i3-msg exit'"

# resize window (you can also use the mouse for that)
mode "resize" {
        # These bindings trigger as soon as you enter the resize mode

        # Pressing left will shrink the window’s width.
        # Pressing right will grow the window’s width.
        # Pressing up will shrink the window’s height.
        # Pressing down will grow the window’s height.
        bindsym j resize shrink width 10 px or 10 ppt
        bindsym k resize grow height 10 px or 10 ppt
        bindsym l resize shrink height 10 px or 10 ppt
        bindsym semicolon resize grow width 10 px or 10 ppt

        # same bindings, but for the arrow keys
        bindsym Left resize shrink width 10 px or 10 ppt
        bindsym Down resize grow height 10 px or 10 ppt
        bindsym Up resize shrink height 10 px or 10 ppt
        bindsym Right resize grow width 10 px or 10 ppt

        # back to normal: Enter or Escape or $mod+r
        bindsym Return mode "default"
        bindsym Escape mode "default"
        bindsym $mod+r mode "default"
}

bindsym $mod+r mode "resize"

# Start i3bar to display a workspace bar (plus the system information i3status
# finds out, if available)
bar {
        tray_padding 4	
        font pango:Ubuntu Mono Bold 11
        strip_workspace_numbers yes
        workspace_min_width 70
        colors {
        statusline #ff88ff
        background #7733ff33
        separator #440055
# | class          |frame    |fill   |text	|
focused_workspace  #ff88ff00 #ff88ff #000000
active_workspace   #333333   #333333 #888888
inactive_workspace #ff88ff00 #220055 #ff88ff
urgent_workspace   #ffaa44   #000000 #ffaa44
        }
        separator_symbol "  "
		position top
		i3bar_command i3bar --transparency
        status_command i3status
}
#############################################################
# after you installed i3 (preferably the "i3" package, not "i3wm")
#
# To run the i3wm like a full desktop environment you need additional components
# if on debian/ubuntu, run this command to install all recommended packages:
# sudo apt install network-manager-gnome nitrogen picom blueman dunst
# after that ensure this i3 config file is located in /home/*username*/.config/i3/config

# copy this file "config" to: ~/.config/i3/
# copy i3status configuration file into the /etc folder
# copy "picom.conf" file into the /etc folder
#################################################################
#					 autostart apps:							#
# "--no-startup-id" fixes unnecessary cursor "loading"			#
# "exec_always" restarts app with every window-manager restart  #
#################################################################

# 	run wifi tray. You can use nm-applet(gtk) or nm-tray(qt)
exec --no-startup-id nm-tray

#	run wallpaper setter
exec --no-startup-id nitrogen --restore
exec --no-startup-id picom --config ~/.config/picom.conf --experimental-backends --corner-radius 25
exec --no-startup-id volumeicon
exec --no-startup-id blueman-applet
exec --no-startup-id dunst
exec --no-startup-id onboard
#	run touch controls
exec --no-startup-id tint2

#	I dont like when screen turns black while watching a movie, so:
#	screen saver here is being turned off:
exec --no-startup-id xset s off 
exec --no-startup-id xset -dpms
exec --no-startup-id xset s noblank

#	Set xrandr eDP1 scaling mode to fill the screen no black bars
exec --no-startup-id xrandr --output eDP1 --set "scaling mode" "Full"
