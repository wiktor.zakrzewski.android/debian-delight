#!/bin/bash
# due to inability of lazy.spawn in qtile to do 2 commands
# I had to make this script to conveniently
# restart picom and tint2panel, which cannot redraw blur.
picom --vsync &
sleep 1 &&
killall tint2 &&
tint2
