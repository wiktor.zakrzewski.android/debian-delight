#!/usr/bin/env python
# -*- coding: utf-8 -*-
import os
import subprocess
from libqtile import bar, layout, widget, hook, extension
from spiral import Spiral
from libqtile.config import Click, Drag, Group, Key, Match, Screen
from libqtile.lazy import lazy
from libqtile.utils import guess_terminal

mod = "mod4"
terminal = "qterminal"  #guess_terminal()
#####################################################################
#                       KEYBINDINGS                                 #
#####################################################################
keys = [
    # A list of available commands that can be bound to keys can be found
    # at https://docs.qtile.org/en/latest/manual/config/lazy.html
    # Switch between windows
    Key([mod], "h", lazy.layout.left(), desc="Move focus to left"),
    Key([mod], "l", lazy.layout.right(), desc="Move focus to right"),
    Key([mod], "j", lazy.layout.down(), desc="Move focus down"),
    Key([mod], "k", lazy.layout.up(), desc="Move focus up"),
    Key([mod], "n", lazy.layout.next(), desc="Move window focus to other window"),
    # Move windows between left/right columns or move up/down in current stack.
    # Moving out of range in Columns layout will create new column.
    Key([mod, "shift"], "h", lazy.layout.shuffle_left(), desc="Move window to the left"),
    Key([mod, "shift"], "l", lazy.layout.shuffle_right(), desc="Move window to the right"),
    Key([mod, "shift"], "j", lazy.layout.shuffle_down(), desc="Move window down"),
    Key([mod, "shift"], "k", lazy.layout.shuffle_up(), desc="Move window up"),
    # Grow windows. If current window is on the edge of screen and direction
    # will be to screen edge - window would shrink.
    Key([mod, "control"], "h", lazy.layout.grow_left(), desc="Grow window to the left"),
    Key([mod, "control"], "l", lazy.layout.grow_right(), desc="Grow window to the right"),
    Key([mod, "control"], "j", lazy.layout.grow_down(), desc="Grow window down"),
    Key([mod, "control"], "k", lazy.layout.grow_up(), desc="Grow window up"),
#    Key([mod], "space", lazy.layout.normalize(), desc="Reset all window sizes"),
    #now with the arrows:
        #Focus:
        Key([mod], "Left", lazy.layout.left(), desc="Move focus to left"),
        Key([mod], "Right", lazy.layout.right(), desc="Move focus to right"),
        Key([mod], "Down", lazy.layout.down(), desc="Move focus down"),
        Key([mod], "Up", lazy.layout.up(), desc="Move focus up"),
        #Moving Windows:
        Key([mod, "shift"], "Left", lazy.layout.shuffle_left(), desc="Move window to the left"),
        Key([mod, "shift"], "Right", lazy.layout.shuffle_right(), desc="Move window to the right"),
        Key([mod, "shift"], "Down", lazy.layout.shuffle_down(), desc="Move window down"),
        Key([mod, "shift"], "Up", lazy.layout.shuffle_up(), desc="Move window up"),
        #Resizing Windows:
        Key([mod, "control"], "Left", lazy.layout.grow_left(), desc="Grow window to the left"),
        Key([mod, "control"], "Right", lazy.layout.grow_right(), desc="Grow window to the right"),
        Key([mod, "control"], "Down", lazy.layout.grow_down(), desc="Grow window down"),
        Key([mod, "control"], "Up", lazy.layout.grow_up(), desc="Grow window up"),
    # Toggle between split and unsplit sides of stack.
    # Split = all windows displayed
    # Unsplit = 1 window displayed, like Max layout, but still with

    # toggle floating
    Key([mod], "space", lazy.window.toggle_floating(), desc='Toggle floating'), 
    # toggle indo into FULLSCREEN
    Key([mod], "f", lazy.window.toggle_fullscreen(), desc='toggle fullscreen'),
    # multiple stack panes
    Key(
        [mod, "shift"],
        "Return",
        lazy.layout.toggle_split(),
        desc="Toggle between split and unsplit sides of stack",
    ),
    Key([mod], "t", lazy.spawn(terminal), desc="Launch terminal"),
    #Kill picom for gaming:
    Key([mod, "shift"], "p", lazy.spawn("killall picom"), desc="killall picom"),
    #Respawn picom after gaming:
    Key([mod], "p", lazy.spawn('bash .config/qtile/qtile-restart-picom.sh'), desc="Launch picom with kawase blurring"),
        #picom --experimental-backends --vsync --corner-radius 22
    # Toggle between different layouts as defined below
    Key([mod], "Tab", lazy.next_layout(), desc="Toggle between layouts"),
    Key([mod], "q", lazy.window.kill(), desc="Kill focused window"),
    Key([mod, "control"], "r", lazy.reload_config(), desc="Reload the config"),
    Key([mod, "control"], "q", lazy.shutdown(), desc="Shutdown Qtile"),
    Key([mod], "r", lazy.spawncmd(), desc="Spawn a command using a prompt widget"),
    Key([mod], "x", lazy.spawn("jgmenu_run"), desc="spawn jgmenu"),
    Key([mod], "d", lazy.spawn("dmenu_run -b -fn Ubuntu-37:bold -sf #225 -sb #f8f -nb #fff -nf #f8f -w tint2"), desc="spawn dmenu"),
    Key([mod], "w", lazy.spawn("librewolf"), desc="launch web browser librewolf"),
    Key([mod], "m", lazy.spawn("pcmanfm-qt"), desc="launch file manager pcmanfm-qt"),
#   dmenu integration (uses "extension" import)
#  Key([mod], "r", lazy.run_extension(extension.DmenuRun(
#       dmenu_prompt=">",
#       dmenu_font="Mononoki-11",
#       background="#15181a",
#       foreground="#bbaadd",
#       selected_foreground="#fff",
        #dmenu_height=24,  # breaks default dmenu , only supported by some dmenu forks
#   )), desc="Spawn a command using a DmenuRun widget"),
]



#####################################################################
#                           GROUPS/WORKSPACES                       #
#####################################################################
groups = [Group("", layout='columns'),
          Group("", layout='spiral'),
          Group("", layout='monadwide'),
          Group("", layout='monadwide'),
          Group("", layout='columns'),
          Group("", layout='columns'),
          Group("", layout='columns'),
          Group("", layout='columns'),
          Group("", layout='ratio'),
          Group("", layout='floating')]

# Allow MODKEY+[0 through 9] to bind to groups, see https://docs.qtile.org/en/stable/manual/config/groups.html
# MOD4 + index Number : Switch to Group[index]
# MOD4 + shift + index Number : Send active window to another Group
from libqtile.dgroups import simple_key_binder
dgroups_key_binder = simple_key_binder("mod4")

layouts = [
    layout.Columns(
        #border_focus_stack=["#99339933"],
        border_focus="#dd33ff", #["#dd33dd22"],
        border_normal="#333355",
        border_normal_stack="#00ff00",
        border_width=2,
        border_on_single=True,
        margin=7
    ),
    Spiral(
                #border_focus_stack=["#99339933"],
            clockwise=False,
          #      new_client_position="bottom",#top, after_current, before_current
           #     ratio="0.6180469715698392",
            border_focus="#dd33ff", #["#dd33dd22"],
            border_normal="#333355",
                #border_normal_stack="#00ff00",
            border_width=2,
                #border_on_single=True,
            margin=7
    ),    
    # Try more layouts by unleashing below layouts.
    # layout.Stack(num_stacks=2),
    # layout.Bsp(),
    layout.MonadWide(
            border_focus="#dd33ff", #["#dd33dd22"],
            border_normal="#333355",
            border_width=2,
            margin=5,
        ),
    layout.Matrix(
        border_focus="#dd33ff",
        border_normal="#333355",
        border_width=2,
        margin=5,
        columns=3
    ),
    #layout.MonadTall(
        #focus_stack=["#99339933"],
    #    border_focus="#dd33ff00", #["#dd33dd22"],
    #   border_normal="#333355",
        #border_normal_stack="#00ff00",
    #    border_width=2,
    #    border_on_single=True,
    #    margin=5 
    #),
    layout.RatioTile(
        border_focus="#dd33ff", #["#dd33dd22"],
        border_normal="#333355",
        border_width=2,
        margin=2,
    ),
    layout.Max(),
        
    # layout.Tile(),
    # layout.TreeTab(),
    # layout.VerticalTile(),
    # layout.Zoomy(),
]
#####################################################################
#                       BAR n WIDGETS                               #
#####################################################################
widget_defaults = dict(
#    font="Tlwg Mono Bold", <--calkiem fajna techniczna czciona
    font="Hack Bold",
    fontsize=17,
    padding=5,
)
extension_defaults = widget_defaults.copy()

screens = [
    Screen(
        bottom=bar.Bar(
            [   
                #widget.Spacer(length=5),
                widget.Image(
                    filename = "/home/cliff/Debian-Logo-thick-smooth2-light2.png",
                    margin_x = 6,
                    margin_y = 6,
                    mouse_callbacks = {'Button1': lazy.spawn("jgmenu_run")}
                    ), 
                widget.GroupBox(
                    highlight_method='block',
                    active = '#444444',    #ff9955 old orange color  #active group text color
                    inactive = '#44444444',
                    block_highlight_text_color = '#ffffff88', #highlighted group text color
                    this_current_screen_border= '#444444', # highlight bg color
                    margin_x = 0,
                    margin_y = 3,
                    rounded = True,
                    borderwidth = 2,
                    hide_unused = False
                    ),
             #   widget.Spacer(length=50),
                widget.Prompt(
                    foreground='#000000', 
                    mouse_callbacks = {'Button1': lazy.spawncmd()}
                    ),
                widget.WindowName(
                    mouse_callbacks = {'Button1': lazy.spawncmd()},
                    foreground='#444444'),
             #   widget.Chord(
             #       chords_colors={
             #           "launch": ("#ff00ff", "#ffffff"),
             #       },
             #       name_transform=lambda name: name.upper(),
             #  ),
                widget.Notify(
                    default_timeout=5,
                    foreground='#ff3333',
                    max_chars=45
                    ),
             #   widget.Memory(foreground = '#ff88ff', update_interval = 5, measure_mem='M' ),
             #   widget.Battery(battery="CMB1", foreground='#ffaa00'),
                # widget.ThermalSensor(),
                #widget.TextBox("Press &lt;M-r&gt; to spawn", foreground="#d75f5f"),
                
            #    widget.Cmus(foreground='#ff88ff', max_chars=20, play_color='#ff88ff', noplay_color='#aaaaaa'),
                #if you want a 12h timescale change '%H:%M' to: '%I:%M %p'
            #    widget.Clock(format="%H:%M %a %d-%m-%Y", foreground = '#ff88ff'),
                widget.CurrentLayout(foreground = '#444444'),
            #    widget.QuickExit(foreground = '#aa3388',default_text='[shutdown]' ),
                # widget.Systray(background='#00000000',icon_size=30, padding=16 ),#background='#55005500'),
            #    widget.Spacer(length=25),
                widget.Spacer(length=7),
            ],
            50, background="#ffffff88", 
            # #33333355 deepin-dark: 
            #"#55005500"-old,"#8800dd22"-coolest->bug
            # #ffffff55 - light deepin-like panel for light noisy wallpapers only
            # #00000022 - dark  deepin-like panel
            opacity=1,
            #border_width=[1, 14, 1, 14],
            #border_color="#ffffff55", # dark prpl: "#5544aa", grey: "#333355"
            margin=[0, 979, 8, 6]
            # border_width=[2, 0, 2, 0],  # Draw top and bottom borders
            # border_color=["ff00ff", "000000", "ff00ff", "000000"]  # Borders are magenta
            ),
        ),
]

# Drag floating layouts.
mouse = [
    Drag([mod], "Button1", lazy.window.set_position_floating(), start=lazy.window.get_position()),
    Drag([mod], "Button3", lazy.window.set_size_floating(), start=lazy.window.get_size()),
    Click([mod], "Button2", lazy.window.bring_to_front()),
]
#below default line has to be commented or group/workspace shortcuts won't work:
#dgroups_key_binder = None
dgroups_app_rules = []  # type: list
follow_mouse_focus = True
bring_front_click = False
cursor_warp = False
floating_layout = layout.Floating(
    border_focus="#553388",
    border_normal="#00000055",
    border_width=4,
    float_rules=[
        # Run the utility of `xprop` to see the wm class and name of an X client.
        *layout.Floating.default_float_rules,
        Match(wm_class="confirmreset"),  # gitk
        Match(wm_class="makebranch"),  # gitk
        Match(wm_class="maketag"),  # gitk
        Match(wm_class="ssh-askpass"),  # ssh-askpass
        Match(title="branchdialog"),  # gitk
        Match(title="pinentry"),  # GPG key password entry
    ]
)
auto_fullscreen = True
focus_on_window_activation = "smart"
reconfigure_screens = True

# If things like steam games want to auto-minimize themselves when losing
# focus, should we respect this or not?
auto_minimize = True
#######################################3
#run autostart script
#######################################
@hook.subscribe.startup_once
def autostart():
    home = os.path.expanduser('~/.config/qtile/autostart.sh')
    subprocess.call([home])

# When using the Wayland backend, this can be used to configure input devices.
wl_input_rules = None

# XXX: Gasp! We're lying here. In fact, nobody really uses or cares about this
# string besides java UI toolkits; you can see several discussions on the
# mailing lists, GitHub issues, and other WM documentation that suggest setting
# this string if your java app doesn't work correctly. We may as well just lie
# and say that we're a working one by default.
#
# We choose LG3D to maximize irony: it is a 3D non-reparenting WM written in
# java that happens to be on java's whitelist.
wmname = "LG3D"
