#!/bin/sh
# This script determines number of connected monitors and configures them.

numberofmonitors=$(xrandr|grep -o -c " connected")

case $numberofmonitors in

# Single monitor
  1)  xrandr --auto;;

# Multiple monitors
  *)  listofmonitors=$(xrandr|grep -o ^.*\ connected|grep -o ^.*\ )

      # Get monitor names
      firstmonitor=$(echo $listofmonitors|grep -o ^.*\ )
      lastmonitor=$(echo $listofmonitors|grep -o \ .*$)

      # Disable first and enable last monitor
      # You can change --off or --auto options as you wish
      xrandr --output $firstmonitor --off \
             --output $lastmonitor --auto;;
esac
