#this is a script that will load up xsetwacom settings for you to be able to use the huion
#stylus with your wacom screen

#First you have to turn off the tip close up button 2 push
#the huion stylus has error, when you put tip near the screen
#it pushes the button 2 (middle mouse button)
#so we will redirect it to push button 0 (no button)
xsetwacom --set "Wacom MultiTouch Sensor Pen stylus" Button 2 "button 0"
