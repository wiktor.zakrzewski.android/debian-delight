#!/bin/bash
#Detectmon.sh
#this is the first script of 2 scripts
#its purpose is to automatically detect if
#a hdmi monitor is connected and to run another script
#switchview.sh
#that prompts you what to do

#XRANDR way do detect HDMI state:(uncomment to use)
# hdmi_active=$(xrandr |grep ' connected' |grep 'HDMI' |awk '{print $1}')
#then use sth like:
# [[ ! -z "$hdmi_active" ]] && bash switchview.sh

#HDDMI STATUS FILE way:
#to test:
# cat /sys/class/drm/card0/*HDMI*/status

hdmi_active="$(cat /sys/class/drm/card0/*HDMI*/status |grep '^connected')" #Using ^ we avo
 [[ ! -z "$hdmi_active" ]] && bash switchview.sh #hdmi is active
